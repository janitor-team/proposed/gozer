%define name    gozer
%define ver     0.7
%define RELEASE 1
#%define rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define rel     1
%define prefix  /usr

Summary: A command-line utility for rendering images of text with AA and TTF support
Name: %{name}
Version: %{ver}
Release: %{rel}
Copyright: BSD
Group: User Interface/X
URL: http://linuxbrit.co.uk/
Packager: Term <term@twistedpath.org>
Vendor: The Enlightenment Development Team <e-develop@enlightenment.org>
Source: ftp://ftp.enlightenment.org/enlightenment/%{name}-%{ver}.tar.gz
BuildRoot: /var/tmp/%{name}-root
Requires: imlib2 >= 1.0.0
Requires: giblib

%description
gozer is a commandline text rendering utility for creating images from
abitrary text in antialised truetype fonts using optional fontstyles,
wordwrapping and layout con� trol. gozer uses the imlib2 library to create
and save images.

Install gozer to be able to create images with anti-alias and truetype font
support from the commandline.

%prep
%setup -q

%build
if [ ! -e ./configure ]
then
  ./autogen.sh --prefix=%{prefix}
else
  ./configure --prefix=%{prefix}
fi
make

%install
make prefix=$RPM_BUILD_ROOT%{prefix} install

#%post

#%postun

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README AUTHORS ChangeLog TODO
%defattr(-,root,root)
%{prefix}/share/gozer/*
%{prefix}/bin/gozer
%{prefix}/man/man1/*

%changelog
* Sun Nov 12 2000 Term <term@twistedpath.org>
- rewrote spec file :P
- added contrib stuffs

* Thu Oct 26 2000 Tom Gilbert <tom@linuxbrit.co.uk>
- created spec file
